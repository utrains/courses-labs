# docker-compose haproxy

##Easy HAproxy using docker-compose with three web (html) applications:

### Step to build these containers (haproxy, three httpd container that contains three web app):

- 1- clone the repository;
- 2- docker-compose-haproxy folders, you will see four (04) folders (web-a; web-b; web-c and haproxy). when you open each of these folders, there is a Dockerfile, which will be responsible for generating the docker image when we run our docker-compose up command. 
- 3- return to the docker-compose-haproxy folder , open the docker-compose.yml file that contains docker-compose instructions to deploy the whole project;
- 4- in your terminal, make juste this command : sudo docker-compose up -d --build. 

```
$ git clone git@gitlab.com:utrains/courses-labs.git
$ cd courses-labs/docker-compose-haproxy/
$ docker-compose up -d --build
```
#### Check your browser, or make the sudo docker ps command

```
$ docker ps
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                NAMES
9c4d73849495        dockerhaproxy_haproxy        "/docker-entrypoin..."   2 seconds ago       Up 2 seconds        0.0.0.0:80->80/tcp   haproxy
d339129f080e        dockerhaproxy_apache-php-2   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-2
b4630777daf4        dockerhaproxy_apache-php-1   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-1
b4630777daf4        dockerhaproxy_apache-php-3   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-3
```
#### if all is ok, congratulation ! you're build haproxy using docker-compose.
