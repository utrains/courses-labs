# Lab : HAProxy, Docker, httpd, docker-compose


##In this lab, we want to achieve high availability of web applications:

we need these tools:
- HAProxy container
- the web application that we want to deploy.


## Step 1: creation of folders
in this step, we will create a first **lab_haproxy_1** folder which will contain all the project folders. After that open that folder.<br/> Create the 4 new folders: **config, web_app_1, web_app_2** and **web_app_3**

```// in ubuntu server

mkdir lab_haproxy_1
mkdir config
mkdir web_app_1
mkdir web_app_2
mkdir web_app_3
```

## Step 1: creation of files in each folder: 
- [ ] In web_app_1  make this instructions :

```// in ubuntu server

cd web_app_1
cp /path/for/logo/utrains/utrain-logo.png . // this command copies the logo of utrain in this directory
touch index.html

// In the index.html file, put these html code instruction:
<!DOCTYPE html>
<html lang= ‘en’>
    <head>
        <meta charset=’UTF-8’>
	    <title>HAProxy</title>
        <style>
            div {
                background-color: rgb(25, 80, 34);
                height: 200px;
                width: 500px;           
            }
        </style>
    </head>
    <body>
        <div>
            <h1 style="color:rgb(227, 240, 241);"> Hello Utrains Team </h1> </br>
            <h1 style="color:rgb(227, 240, 241);"> HAProxy in action !!!</br> Web APP 1 </h1>
        </div>
        
            </br>
        </p>
        <!--This line display the Utrains Logo to the html page-->
        <img src="utrain_logo.png" alt="Italian Trulli">
    </body>
</html>
```

- [ ] repeat this process in the web_app_2 and web_app_3 folders:
this allows to see during the tests how each application is launched

## Step 2: repeat this process in the web_app_2 and web_app_3 folders : This allows to see during the tests how each application is launched.


## Step 3: configure the HAProxy server: haproxy.cfg file

[More information about haproxy config]
(https://www.haproxy.com/blog/the-four-essential-sections-of-an-haproxy-configuration/)

```// config haproxy.cfg
cd config 
touch haproxy.cfg

#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   http://haproxy.1wt.eu/download/1.4/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local2

 #   chroot      /var/lib/haproxy
    pidfile     /usr/local/etc/haproxy/haproxy.pid
    maxconn     4000
#    user        haproxy
#   group       haproxy
    daemon

    # turn on stats unix socket
  #  stats socket /var/lib/haproxy/stats
    stats socket /usr/local/etc/haproxy/stats

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------
frontend  stats
     bind *:8080
#    acl url_static       path_beg       -i /static /images /javascript /stylesheets
#    acl url_static       path_end       -i .jpg .gif .png .css .js

#    use_backend static          if url_static
    default_backend             app
     stats enable
     stats auth admin:password
     stats hide-version
     stats show-node
     stats refresh 60s
     stats uri /haproxy?stats
#---------------------------------------------------------------------
# static backend for serving up images, stylesheets and such
#---------------------------------------------------------------------
#backend static
#    balance     roundrobin
#    server      static 127.0.0.1:4331 check

#---------------------------------------------------------------------
# round robin balancing between the various backends
#---------------------------------------------------------------------
backend app
    balance     roundrobin
    server  webserver1 webserver1:80 check
    server  webserver2 webserver2:80 check
    server  webserver3 webserver3:80 check

#    server  app3 127.0.0.1:5003 check
#    server  app4 127.0.0.1:5004 check
```

## Step 4: Make the docker command to start web server.


- [ ] *Create the Three (03) webserver containers :*

```//in ubuntu server prompt:
sudo docker run -d --name webserver1 -v /home/hermann90/utrainsProjects/courses-labs/web_app_1:/usr/local/apache2/htdocs -p 86:80 httpd
sudo docker run -d --name webserver2 -v /home/hermann90/utrainsProjects/courses-labs/web_app_2:/usr/local/apache2/htdocs -p 86:80 httpd
sudo docker run -d --name webserver3 -v /home/hermann90/utrainsProjects/courses-labs/web_app_3:/usr/local/apache2/htdocs -p 86:80 httpd
```

start your browser and check if these web applications display and run normally.

## Step 4: Make the docker command to start HAProxy server.
```//in ubuntu server prompt:
sudo docker run -d --name haproxy --link webserver1:webserver1 --link webserver2:webserver2 --link webserver3:webserver3 -v /home/hermann90/utrainsProjects/courses-labs/config:/usr/local/etc/haproxy -p 90:8080 haproxy
```


start your browser in the port that haproxy is listening, refresh a see how the load balancer working with the three web server.


## To GO FASTER:
- 1-clone the project;
- 2-position yourself in the project repository;
- 3-use docker to run each web application;
- 4-launch the haproxy container while linking the different applications with the --linke option.

```// In Linux terminal:

git clone https://gitlab.com/utrains/courses-labs.git or git clone git@gitlab.com:utrains/courses-labs.git
cd courses-labs
git fetch origin UTRAINS-HAPROXY-LABS-1
git checkout UTRAINS-HAPROXY-LABS-1
sudo docker run -d --name webserver1 -v /home/hermann90/utrainsProjects/courses-labs/web_app_1:/usr/local/apache2/htdocs -p 86:80 httpd
sudo docker run -d --name webserver2 -v /home/hermann90/utrainsProjects/courses-labs/web_app_2:/usr/local/apache2/htdocs -p 86:80 httpd
sudo docker run -d --name webserver3 -v /home/hermann90/utrainsProjects/courses-labs/web_app_3:/usr/local/apache2/htdocs -p 86:80 httpd
sudo docker run -d --name haproxy --link webserver1:webserver1 --link webserver2:webserver2 --link webserver3:webserver3 -v /home/hermann90/utrainsProjects/courses-labs/config:/usr/local/etc/haproxy -p 90:8080 haproxy```
start your browser in the port that haproxy is listening, refresh a see how the load balancer working with the three web server.
```

## II- deploy these App, using docker-compose: Let us know if you have any problems doing this Lab

# docker-compose haproxy

##Easy HAproxy using docker-compose with three web (html) applications:

### Step to build these containers (haproxy, three httpd container that contains three web app):

- 1- clone the repository;
- 2- docker-compose-haproxy folders, you will see four (04) folders (web-a; web-b; web-c and haproxy). when you open each of these folders, there is a Dockerfile, which will be responsible for generating the docker image when we run our docker-compose up command. 
- 3- return to the docker-compose-haproxy folder , open the docker-compose.yml file that contains docker-compose instructions to deploy the whole project;
- 4- in your terminal, make juste this command : sudo docker-compose up -d --build. 

```
$ git clone git@gitlab.com:utrains/courses-labs.git
$ cd courses-labs/docker-compose-haproxy/
$ docker-compose up -d --build
```
#### Check your browser, or make the sudo docker ps command

```
$ docker ps
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                NAMES
9c4d73849495        dockerhaproxy_haproxy        "/docker-entrypoin..."   2 seconds ago       Up 2 seconds        0.0.0.0:80->80/tcp   haproxy
d339129f080e        dockerhaproxy_apache-php-2   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-2
b4630777daf4        dockerhaproxy_apache-php-1   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-1
b4630777daf4        dockerhaproxy_apache-php-3   "docker-php-entryp..."   2 seconds ago       Up 2 seconds        80/tcp               apache-php-3
```
#### if all is ok, congratulation ! you're build haproxy using docker-compose.




